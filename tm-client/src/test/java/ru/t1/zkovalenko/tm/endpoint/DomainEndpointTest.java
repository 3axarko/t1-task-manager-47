package ru.t1.zkovalenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.endpoint.IDomainEndpoint;
import ru.t1.zkovalenko.tm.api.endpoint.IProjectEndpoint;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.dto.model.ProjectDTO;
import ru.t1.zkovalenko.tm.dto.request.data.*;
import ru.t1.zkovalenko.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.zkovalenko.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.zkovalenko.tm.dto.request.project.ProjectShowByIdRequest;
import ru.t1.zkovalenko.tm.marker.IntegrationCategory;
import ru.t1.zkovalenko.tm.service.PropertyService;

import static ru.t1.zkovalenko.tm.constant.EndpointTestData.USER_ADMIN_LOGIN;
import static ru.t1.zkovalenko.tm.constant.EndpointTestData.USER_ADMIN_PASSWORD;

@Category(IntegrationCategory.class)
public class DomainEndpointTest extends AbstractEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @Nullable
    private String userToken;

    @NotNull
    private ProjectDTO testProjectCreate() {
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken);
        createRequest.setName("PROJECT_NAME");
        createRequest.setDescription("Description");
        @Nullable final ProjectDTO projectCreated = projectEndpoint.createProject(createRequest).getProject();
        Assert.assertNotNull(projectCreated);
        return projectCreated;
    }

    @NotNull
    private ProjectDTO projectFind(final ProjectDTO projectCreated) {
        @NotNull final ProjectShowByIdRequest showByIdRequest = new ProjectShowByIdRequest(userToken);
        showByIdRequest.setProjectId(projectCreated.getId());
        @Nullable final ProjectDTO projectFounded = projectEndpoint.showByIdProject(showByIdRequest).getProject();
        Assert.assertNotNull(projectFounded);
        return projectFounded;
    }

    @Before
    public void before() {
        userToken = login(USER_ADMIN_LOGIN, USER_ADMIN_PASSWORD);
        serverAutoBackup(false, userToken);
    }

    @After
    public void after() {
        loadBackup(userToken);
        serverAutoBackup(true, userToken);
        logout(userToken);
    }

    @Test
    public void dataBackupSaveLoad() {
        @NotNull final ProjectDTO projectCreated = testProjectCreate();
        @NotNull final DataBackupSaveRequest requestSave = new DataBackupSaveRequest(userToken);
        domainEndpoint.backupSaveData(requestSave);
        loadBackup(userToken);
        @NotNull final ProjectDTO projectFounded = projectFind(projectCreated);
        Assert.assertEquals(projectCreated.getId(), projectFounded.getId());
        @NotNull final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(userToken);
        removeByIdRequest.setProjectId(projectCreated.getId());
        projectEndpoint.removeByIdProject(removeByIdRequest);
        domainEndpoint.backupSaveData(requestSave);
    }

    @Test
    public void dataBase64SaveLoad() {
        @NotNull final ProjectDTO projectCreated = testProjectCreate();
        @NotNull final DataBase64SaveRequest requestSave = new DataBase64SaveRequest(userToken);
        domainEndpoint.base64SaveData(requestSave);
        @NotNull final DataBase64LoadRequest requestLoad = new DataBase64LoadRequest(userToken);
        domainEndpoint.base64LoadData(requestLoad);
        @NotNull final ProjectDTO projectFounded = projectFind(projectCreated);
        Assert.assertEquals(projectCreated.getId(), projectFounded.getId());
    }

    @Test
    public void dataBinarySaveLoad() {
        @NotNull final ProjectDTO projectCreated = testProjectCreate();
        @NotNull final DataBinarySaveRequest requestSave = new DataBinarySaveRequest(userToken);
        domainEndpoint.binarySaveData(requestSave);
        @NotNull final DataBinaryLoadRequest requestLoad = new DataBinaryLoadRequest(userToken);
        domainEndpoint.binaryLoadData(requestLoad);
        @NotNull final ProjectDTO projectFounded = projectFind(projectCreated);
        Assert.assertEquals(projectCreated.getId(), projectFounded.getId());
    }

    @Test
    public void dataJsonSaveLoadJFasterXml() {
        @NotNull final ProjectDTO projectCreated = testProjectCreate();
        @NotNull final DataJsonSaveFasterXmlRequest requestSave = new DataJsonSaveFasterXmlRequest(userToken);
        domainEndpoint.jsonSaveFasterXmlData(requestSave);
        @NotNull final DataJsonLoadJFasterXmlRequest requestLoad = new DataJsonLoadJFasterXmlRequest(userToken);
        domainEndpoint.jsonLoadJFasterXmlData(requestLoad);
        @NotNull final ProjectDTO projectFounded = projectFind(projectCreated);
        Assert.assertEquals(projectCreated.getId(), projectFounded.getId());
    }

    @Test
    public void dataJsonSaveLoadJaxB() {
        @NotNull final ProjectDTO projectCreated = testProjectCreate();
        @NotNull final DataJsonSaveJaxBRequest requestSave = new DataJsonSaveJaxBRequest(userToken);
        domainEndpoint.jsonSaveJaxBData(requestSave);
        @NotNull final DataJsonLoadJaxBRequest requestLoad = new DataJsonLoadJaxBRequest(userToken);
        domainEndpoint.jsonLoadJaxBData(requestLoad);
        @NotNull final ProjectDTO projectFounded = projectFind(projectCreated);
        Assert.assertEquals(projectCreated.getId(), projectFounded.getId());
    }

    @Test
    @Ignore //вместо null загружается пустое значение
    public void dataXmlSaveLoadFasterXml() {
        @NotNull final ProjectDTO projectCreated = testProjectCreate();
        @NotNull final DataXmlSaveFasterXmlRequest requestSave = new DataXmlSaveFasterXmlRequest(userToken);
        domainEndpoint.xmlSaveFasterXmlData(requestSave);
        @NotNull final DataXmlLoadJFasterXmlRequest requestLoad = new DataXmlLoadJFasterXmlRequest(userToken);
        domainEndpoint.xmlLoadJFasterXmlData(requestLoad);
        @NotNull final ProjectDTO projectFounded = projectFind(projectCreated);
        Assert.assertEquals(projectCreated.getId(), projectFounded.getId());
    }

    @Test
    public void dataXmlSaveLoadJaxB() {
        @NotNull final ProjectDTO projectCreated = testProjectCreate();
        @NotNull final DataXmlSaveJaxBRequest requestSave = new DataXmlSaveJaxBRequest(userToken);
        domainEndpoint.xmlSaveJaxBData(requestSave);
        @NotNull final DataXmlLoadJaxBRequest requestLoad = new DataXmlLoadJaxBRequest(userToken);
        domainEndpoint.xmlLoadJaxBData(requestLoad);
        @NotNull final ProjectDTO projectFounded = projectFind(projectCreated);
        Assert.assertEquals(projectCreated.getId(), projectFounded.getId());
    }

    @Test
    public void dataYamlSaveLoadFasterXml() {
        @NotNull final ProjectDTO projectCreated = testProjectCreate();
        @NotNull final DataYamlSaveFasterXmlRequest requestSave = new DataYamlSaveFasterXmlRequest(userToken);
        domainEndpoint.yamlSaveFasterXmlData(requestSave);
        @NotNull final DataYamlLoadFasterXmlRequest requestLoad = new DataYamlLoadFasterXmlRequest(userToken);
        domainEndpoint.yamlLoadFasterXmlData(requestLoad);
        @NotNull final ProjectDTO projectFounded = projectFind(projectCreated);
        Assert.assertEquals(projectCreated.getId(), projectFounded.getId());
    }

}
