package ru.t1.zkovalenko.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    M update(@NotNull M model);

    @NotNull Collection<M> add(@NotNull Collection<M> models);

    @NotNull Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    M remove(M model);

    boolean existById(@NotNull String id);

    int getSize();

    @NotNull List<M> findAll();

    @NotNull List<M> findAll(@Nullable Comparator comparator);

    void clear();

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

}
