package ru.t1.zkovalenko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.model.IUserRepository;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.util.HashUtil;

import javax.persistence.EntityManager;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return this.getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<User> getClazz() {
        return User.class;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password
    ) {
        return create(propertyService, login, password, null, null);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) {
        return create(propertyService, login, password, email, null);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        return create(propertyService, login, password, null, role);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String saltPass = HashUtil.salt(propertyService, password);
        user.setPasswordHash(saltPass == null ? "" : saltPass);
        if (role != null) user.setRole(role);
        if (email != null) user.setEmail(email);
        return add(user);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = String.format("FROM %s where login = :login", getEntityName());
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("login", login)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = String.format("FROM %s where email = :email", getEntityName());
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("email", email)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
