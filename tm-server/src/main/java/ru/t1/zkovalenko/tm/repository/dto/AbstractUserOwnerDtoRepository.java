package ru.t1.zkovalenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.dto.IUserOwnerDtoRepository;
import ru.t1.zkovalenko.tm.dto.model.AbstractUserOwnerModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnerDtoRepository<M extends AbstractUserOwnerModelDTO>
        extends AbstractDtoRepository<M> implements IUserOwnerDtoRepository<M> {

    public AbstractUserOwnerDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        add(model);
        return model;
    }

    @Override
    public boolean existById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = String.format("SELECT COUNT(1) FROM %s where user_id = :userId", getEntityName());
        @NotNull final Long cnt = entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
        return Math.toIntExact(cnt);
    }

    @Override
    public @NotNull List<M> findAll(@Nullable final String userId) {
        return findAll(userId, null);
    }

    @Override
    public @NotNull List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        @NotNull final String jpql = String.format("FROM %s WHERE user_id = :userId ORDER BY :sort", getEntityName());
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setParameter("sort", getOrderByField(comparator))
                .getResultList();
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final String jpql = String.format("DELETE FROM %s where user_id = :userId", getEntityName());
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final String jpql = String.format("FROM %s WHERE id = :id and user_id = :userId", getEntityName());
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        @NotNull final String jpql = String.format("FROM %s where user_id = :userId ORDER BY :sort", getEntityName());
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setParameter("sort", getOrderByField())
                .setFirstResult(index - 1)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

}