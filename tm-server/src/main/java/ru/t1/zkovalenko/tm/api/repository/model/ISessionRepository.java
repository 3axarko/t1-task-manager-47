package ru.t1.zkovalenko.tm.api.repository.model;

import ru.t1.zkovalenko.tm.model.Session;

public interface ISessionRepository extends IUserOwnerRepository<Session> {

}
