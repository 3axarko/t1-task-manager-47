package ru.t1.zkovalenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.zkovalenko.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskDtoRepository extends AbstractUserOwnerDtoRepository<TaskDTO> implements ITaskDtoRepository {

    public TaskDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return this.getClazz().getSimpleName();
    }

    @NotNull
    @Override
    protected Class<TaskDTO> getClazz() {
        return TaskDTO.class;
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable final String userId,
                          @NotNull final String name,
                          @Nullable final String description) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description == null ? "" : description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = String.format("FROM %s where project_Id = :projectId", getEntityName());
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId) {
        @NotNull final String jpql = String.format("DELETE FROM %s where project_id = :projectId", getEntityName());
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

}