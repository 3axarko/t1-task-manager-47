package ru.t1.zkovalenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.constant.TableConstant;
import ru.t1.zkovalenko.tm.api.repository.dto.IDtoRepository;
import ru.t1.zkovalenko.tm.comparator.CreatedComparator;
import ru.t1.zkovalenko.tm.comparator.NameComparator;
import ru.t1.zkovalenko.tm.comparator.StatusComparator;
import ru.t1.zkovalenko.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IDtoRepository<M> {

    public AbstractDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected abstract String getEntityName();

    @NotNull
    protected abstract Class<M> getClazz();

    @NotNull
    protected final EntityManager entityManager;

    @NotNull
    protected final String getOrderByField(@Nullable final Comparator comparator) {
        if (CreatedComparator.INSTANCE.equals(comparator)) return TableConstant.FIELD_CREATED;
        if (StatusComparator.INSTANCE.equals(comparator)) return TableConstant.FIELD_STATUS;
        if (NameComparator.INSTANCE.equals(comparator)) return TableConstant.FIELD_NAME;
        else return TableConstant.FIELD_ID;
    }

    @NotNull
    protected final String getOrderByField() {
        return getOrderByField(null);
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public M update(@NotNull final M model) {
        return entityManager.merge(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        entityManager.remove(model);
        return model;
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = String.format("SELECT COUNT(1) FROM %s", getEntityName());
        @NotNull TypedQuery<Long> typedQuery = entityManager.createQuery(jpql, Long.class);
        if ("User".equals(getEntityName())) typedQuery.setHint("org.hibernate.cacheable", true);
        @NotNull final Long cnt = typedQuery.getSingleResult();
        return Math.toIntExact(cnt);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return findAll(null);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable Comparator comparator) {
        @NotNull final String jpql = String.format("FROM %s ORDER BY :sort", getEntityName());
        @NotNull TypedQuery<M> typedQuery = entityManager.createQuery(jpql, getClazz())
                .setParameter("sort", getOrderByField(comparator));
        if ("User".equals(getEntityName())) typedQuery.setHint("org.hibernate.cacheable", true);
        return typedQuery.getResultList();
    }

    @Override
    public void clear() {
        @NotNull final String jpql = String.format("DELETE FROM %s", getEntityName());
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull String id) {
        @NotNull final String jpql = String.format("FROM %s WHERE id = :id", getEntityName());
        @NotNull TypedQuery<M> typedQuery = entityManager.createQuery(jpql, getClazz())
                .setParameter("id", id);
        if ("User".equals(getEntityName())) typedQuery.setHint("org.hibernate.cacheable", true);
        return typedQuery.getResultList()
                .stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull Integer index) {
        @NotNull final String jpql = String.format("FROM %s ORDER BY :sort", getEntityName());
        @NotNull TypedQuery<M> typedQuery = entityManager.createQuery(jpql, getClazz())
                .setParameter("sort", getOrderByField())
                .setFirstResult(index - 1)
                .setMaxResults(1);
        if ("User".equals(getEntityName())) typedQuery.setHint("org.hibernate.cacheable", true);
        return typedQuery.getResultList()
                .stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M removeById(@NotNull String id) {
        M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull Integer index) {
        M model = findOneByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

}
