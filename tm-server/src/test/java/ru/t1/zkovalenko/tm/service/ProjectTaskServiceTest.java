package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.model.IProjectService;
import ru.t1.zkovalenko.tm.api.service.model.IProjectTaskService;
import ru.t1.zkovalenko.tm.api.service.model.ITaskService;
import ru.t1.zkovalenko.tm.api.service.model.IUserService;
import ru.t1.zkovalenko.tm.marker.UnitServiceCategory;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.service.model.ProjectService;
import ru.t1.zkovalenko.tm.service.model.ProjectTaskService;
import ru.t1.zkovalenko.tm.service.model.TaskService;
import ru.t1.zkovalenko.tm.service.model.UserService;

import java.util.List;

import static ru.t1.zkovalenko.tm.constant.model.ProjectTestData.PROJECT1;
import static ru.t1.zkovalenko.tm.constant.model.ProjectTestData.PROJECT_NAME;
import static ru.t1.zkovalenko.tm.constant.model.TaskTestData.TASK1;
import static ru.t1.zkovalenko.tm.constant.model.TaskTestData.TASK_NAME;
import static ru.t1.zkovalenko.tm.constant.model.UserTestData.USER_LIST;

@Category(UnitServiceCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(
            connectionService,
            propertyService
    );

    @Nullable
    private Project projectCreated;

    @Nullable
    private Task taskCreated;

    @Nullable
    private Task taskFounded;

    @Nullable
    private User userCreated;

    @Before
    public void addProjectTask() {
        userCreated = userService.add(USER_LIST.get(0));
        PROJECT1.setUser(userCreated);
        PROJECT1.setName(PROJECT_NAME);
        projectCreated = projectService.add(PROJECT1);
        TASK1.setUser(userCreated);
        TASK1.setName(TASK_NAME);
        taskCreated = taskService.add(TASK1);
        projectTaskService.bindTaskToProject(userCreated.getId(), projectCreated.getId(), taskCreated.getId());
        taskFounded = taskService.findAllByProjectId(userCreated.getId(), projectCreated.getId()).get(0);
    }

    @After
    public void after() {
        if (taskCreated != null) taskService.remove(taskCreated);
        taskCreated = null;
        if (projectCreated != null) projectService.remove(projectCreated);
        projectCreated = null;
        userService.remove(userCreated);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertEquals(taskCreated.getId(), taskFounded.getId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertNotNull(taskFounded);
        projectTaskService.removeProjectById(userCreated.getId(), projectCreated.getId());
        Assert.assertNull(taskService.findOneById(taskFounded.getId()));
        projectCreated = null;
    }

    @Test
    public void removeProjectByIndex() {
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull Integer index = 1;
        for (Project project : projects) {
            if (project.getId().equals(projectCreated.getId())) break;
            index++;
        }
        projectTaskService.removeProjectByIndex(userCreated.getId(), index);
        Assert.assertNull(taskService.findOneById(taskFounded.getId()));
        taskCreated = null;
        projectCreated = null;
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertNotNull(taskFounded);
        projectTaskService.unbindTaskFromProject(userCreated.getId(), projectCreated.getId(), taskCreated.getId());
        final int taskSize = taskService.findAllByProjectId(userCreated.getId(), projectCreated.getId()).size();
        Assert.assertEquals(0, taskSize);
    }

}
