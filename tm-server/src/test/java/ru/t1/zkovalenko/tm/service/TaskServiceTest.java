package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.model.ITaskService;
import ru.t1.zkovalenko.tm.api.service.model.IUserService;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.marker.UnitServiceCategory;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.service.model.TaskService;
import ru.t1.zkovalenko.tm.service.model.UserService;

import static ru.t1.zkovalenko.tm.constant.model.TaskTestData.*;
import static ru.t1.zkovalenko.tm.constant.model.UserTestData.USER_LIST;
import static ru.t1.zkovalenko.tm.enumerated.Status.COMPLETED;
import static ru.t1.zkovalenko.tm.enumerated.Status.NOT_STARTED;

@Category({UnitCategory.class, UnitServiceCategory.class})
public final class TaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(
            connectionService,
            propertyService
    );

    @Nullable
    private User userCreated;

    @Nullable
    private Task taskCreated;

    @Before
    public void before() {
        userCreated = userService.add(USER_LIST.get(0));
    }

    @After
    public void after() {
        if (taskCreated != null) taskService.remove(taskCreated);
        taskCreated = null;
        userService.remove(userCreated);
    }

    @Test
    public void changeTaskStatusById() {
        TASK1.setUser(userCreated);
        taskCreated = taskService.add(TASK1);
        Assert.assertEquals(taskCreated.getStatus(), NOT_STARTED);
        @NotNull final Task taskChanged = taskService
                .changeTaskStatusById(userCreated.getId(), taskCreated.getId(), COMPLETED);
        Assert.assertEquals(taskChanged.getStatus(), COMPLETED);
    }

    @Test
    public void changeTaskStatusByIndex() {
        TASK1.setUser(userCreated);
        taskCreated = taskService.add(TASK1);
        Assert.assertEquals(taskCreated.getStatus(), NOT_STARTED);
        @NotNull final Task taskChanged = taskService
                .changeTaskStatusByIndex(userCreated.getId(), 1, COMPLETED);
        Assert.assertEquals(taskChanged.getStatus(), COMPLETED);
    }

    @Test
    public void createUserIdName() {
        TASK1.setUser(userCreated);
        TASK1.setName(TASK_NAME);
        taskCreated = taskService.add(TASK1);
        @NotNull final Task taskFounded = taskService.findOneById(taskCreated.getId());
        Assert.assertEquals(TASK_NAME, taskFounded.getName());
    }

    @Test
    public void createUserIdNameDescription() {
        TASK1.setUser(userCreated);
        TASK1.setName(TASK_NAME);
        TASK1.setDescription(TASK_DESCRIPTION);
        taskCreated = taskService.add(TASK1);
        @NotNull final Task taskFounded = taskService.findOneById(taskCreated.getId());
        Assert.assertEquals(TASK_NAME, taskFounded.getName());
        Assert.assertEquals(TASK_DESCRIPTION, taskFounded.getDescription());
    }

    @Test
    public void updateById() {
        TASK1.setUser(userCreated);
        TASK1.setName(TASK_NAME);
        TASK1.setDescription(TASK_DESCRIPTION);
        taskCreated = taskService.add(TASK1);
        taskService.updateById(userCreated.getId(),
                taskCreated.getId(),
                TASK_NAME + "1",
                TASK_DESCRIPTION + "1");
        @NotNull final Task taskFounded = taskService.findOneById(taskCreated.getId());
        Assert.assertEquals(taskFounded.getName(), TASK_NAME + "1");
        Assert.assertEquals(taskFounded.getDescription(), TASK_DESCRIPTION + "1");
    }

    @Test
    public void updateByIndex() {
        TASK1.setUser(userCreated);
        TASK1.setName(TASK_NAME);
        TASK1.setDescription(TASK_DESCRIPTION);
        taskCreated = taskService.add(TASK1);
        @NotNull final Task taskFounded = taskService.findOneByIndex(userCreated.getId(), 1);
        Assert.assertEquals(taskFounded.getName(), TASK_NAME);
        Assert.assertEquals(taskFounded.getDescription(), TASK_DESCRIPTION);
        @NotNull final Task taskChanged = taskService.updateByIndex(userCreated.getId(),
                1,
                TASK_NAME + "1",
                TASK_DESCRIPTION + "1");
        Assert.assertEquals(taskChanged.getName(), TASK_NAME + "1");
        Assert.assertEquals(taskChanged.getDescription(), TASK_DESCRIPTION + "1");
    }

}
