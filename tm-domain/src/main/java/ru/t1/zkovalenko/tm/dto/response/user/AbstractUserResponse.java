package ru.t1.zkovalenko.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.model.UserDTO;
import ru.t1.zkovalenko.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @NotNull
    private UserDTO user;

    public AbstractUserResponse(@NotNull UserDTO user) {
        this.user = user;
    }

}
