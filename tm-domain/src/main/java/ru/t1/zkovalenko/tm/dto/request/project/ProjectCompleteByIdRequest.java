package ru.t1.zkovalenko.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public ProjectCompleteByIdRequest(@Nullable final String token) {
        super(token);
    }

}
