package ru.t1.zkovalenko.tm.exception.system;

import lombok.NoArgsConstructor;
import ru.t1.zkovalenko.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractClassException extends AbstractException {

    public AbstractClassException(String message) {
        super(message);
    }

    public AbstractClassException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractClassException(Throwable cause) {
        super(cause);
    }

    public AbstractClassException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
